﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour
{
    public float force;

    public Material blue;
    public Material orange;

    public bool isBlue;

    public MeshRenderer mr;
    
    private void Start()
    {
        mr = gameObject.GetComponent<MeshRenderer>();
    }
    
    void Update()
    {
        if (isBlue)
        {
            mr.material = blue;
        }
        else
        {
            mr.material = orange;
        }
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Cube")
        {
            if (isBlue)
            {
                var rb = other.gameObject.GetComponent<Rigidbody>();
                rb.AddForce(transform.up * force);
            }
            else
            {
                var rb = other.gameObject.GetComponent<Rigidbody>();
                rb.AddForce(-transform.up * force);
            }
            
        }
    }
}
