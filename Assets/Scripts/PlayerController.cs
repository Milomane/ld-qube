﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    private Vector3 inputs = Vector3.zero;
    
    [Header("Camera")] 
    public float cameraRotationSpeed;
    public Transform cameraPivot;
    public Camera camera;
    private float rotX;
    
    [Header("Character")] 
    public float speed = 10;
    public float jumpForce = 50;
    public Vector3 movDir;

    [Header("Blue")]
    public float reUseTimer = .5f;
    public float bumpForce = 50;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        reUseTimer -= Time.deltaTime;
        inputs = Vector3.zero;
        inputs.x = Input.GetAxis("Horizontal");
        inputs.z = Input.GetAxis("Vertical");
        rb.freezeRotation = true;

        if (Input.GetKeyDown("space"))
        {
            rb.AddForce(new Vector3(0, 1, 0) * jumpForce, ForceMode.Impulse);
        }
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + inputs * speed * Time.fixedDeltaTime);
    }

    public void Bump(float bumpForce, Vector3 forwardVector)
    {
        GetComponent<Rigidbody>().AddForce(forwardVector * bumpForce, ForceMode.Impulse);
    }
    
    public void OnCollisionEnter(Collision hit)
    {
        Debug.Log("je hit");
        if (hit.collider.GetComponent<ColorSurface>())
        {
            Debug.Log("je capte color surface");
            if (hit.collider.GetComponent<ColorSurface>().currentColorId == 1 && reUseTimer <= 0)
            {
                Debug.Log("la surface me bump");
                reUseTimer = .5f;
                Bump(bumpForce, hit.collider.transform.forward);
            }
        }
    }
}
