﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandColorScript : MonoBehaviour
{
    public Color color = Color.white;
    public MeshRenderer renderer;

    private float timer = 3;

    void Update()
    {
        renderer.material.color = color;

        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = Random.Range(.5f, 6f);

            int multiplier = Random.Range(-3, 3);

            StartCoroutine(LerpRotation(transform.localRotation.y + multiplier * 90));
        }
    }

    private IEnumerator LerpRotation(float degree)
    {
        while (Mathf.Abs(degree - transform.localRotation.y) > .1f)
        {
            transform.localRotation = Quaternion.Euler(0, transform.localRotation.y + Mathf.Lerp(transform.localRotation.y, degree, .25f), 0);
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
}
